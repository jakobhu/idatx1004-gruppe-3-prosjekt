#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()

# Write your program here.
ev3.speaker.beep()

YMotor = Motor(Port.A)
XMotor = Motor(Port.B)
penMotor = Motor(Port.C)

YMotor.reset_angle(0)
XMotor.reset_angle(0)
penMotor.reset_angle(0)

stoppingPoints = []
ev3.screen.clear()

while True:
    buttons = ev3.buttons.pressed()
    if Button.UP in buttons:
        ev3.screen.print("x: ", XMotor.angle())
        ev3.screen.print("y: ", YMotor.angle(), "\n")
        ev3.screen.print("pen: ", penMotor.angle(), "\n")
        stoppingPoints.append({"x": XMotor.angle(), "y": YMotor.angle(), "pen": penMotor.angle()})
        ev3.speaker.beep()
        wait(500)
    elif len(buttons) > 1:
        break

if len(stoppingPoints) > 1:
    ev3.screen.print("xRange ="   + str(stoppingPoints[0].get("x"))   + "," + str(stoppingPoints[-1].get("x")  ))
    ev3.screen.print("yRange ="   + str(stoppingPoints[0].get("y"))   + "," + str(stoppingPoints[-1].get("y")  ))
    ev3.screen.print("penRange =" + str(stoppingPoints[0].get("pen")) + "," + str(stoppingPoints[-1].get("pen")))