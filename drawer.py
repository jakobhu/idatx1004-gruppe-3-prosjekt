
import math
from pybricks.tools import wait
from pybricks.parameters import Stop

# Klasse for roboten sin bevegelse i aksene
class Drawer:

    # Konstruktør
    def __init__(self, xBoundary, yBoundary, penBoundary, penMotor, xMotor, yMotor, ev3, flipXY = False):
        self.xBoundary = xBoundary
        self.xMotor = xMotor
        self.yBoundary = yBoundary
        self.yMotor = yMotor
        self.penBoundary = penBoundary
        self.penMotor = penMotor
        self.ev3 = ev3
        self.flipXY = flipXY
        self.previousYMoveDirection = 1

    # Metode for å bevege roboten til et punkt
    def moveToPoint(self, point):
        x = self.xBoundary * point[0]
        y = self.yBoundary * point[1]

        xDistance = round(self.xMotor.angle() - x)
        yDistance = round(self.yMotor.angle() - y)

        if self.previousYMoveDirection*yDistance < 0 and yDistance != 0:
            y += 25*self.previousYMoveDirection
            yDistance += 25
            self.previousYMoveDirection = yDistance < 0

        totalDistance = math.sqrt(xDistance**2 + yDistance**2)
        if totalDistance != 0:
            xSpeed = int(abs(xDistance)/totalDistance * 300)
            ySpeed = int(abs(yDistance)/totalDistance * 300)

            motorWait = xSpeed == 0

            if ySpeed != 0:
                self.yMotor.run_target(
                    ySpeed,
                    y, wait = motorWait
                )

            if xSpeed != 0:
                self.xMotor.run_target(
                    xSpeed,
                    x, wait = True
                )
                    
            wait(500)

    # Metode for å senke pennen
    def lowerPen(self):
        # self.penMotor.run_target(100, 0, wait = True)
        self.penMotor.run_until_stalled(-200, then = Stop.COAST, duty_limit = 35)
        wait(100)
        # self.penMotor.run_time(-300, 300, wait = True)
    
    # Metode for å heve pennen
    def raisePen(self):
        self.penMotor.run_time(300, 300, wait = True)
        