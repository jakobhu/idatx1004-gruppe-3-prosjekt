import pygame
import ctypes

# Program som lar brukeren tegne en strek og lagrer koordinatene til streken i en 3d-liste
def getDrawInstructions():
    pygame.init()
    ctypes.windll.shcore.SetProcessDpiAwareness(True)
    ##Antall oppdateringer i sekundet
    fps = 20

    fpsClock = pygame.time.Clock()

    ##Størrelse på tegnevindu
    width, height = 900, 900

    screen = pygame.display.set_mode((width, height))

    ##Egenskaper til "tegnebørsten"
    drawColor = [0, 0, 0]
    brushSize = 5

    canvasSize = [width, height]
    canvas = pygame.Surface(canvasSize)

    ##Bakgrunnsfarge
    canvas.fill((255, 255, 255))

    ##Instrukser for tegning lagret i en 3d-liste på formen [antall pennløft] [x, y]
    drawInstructions=[]

    drawNumber=-1
    lastx=-1
    lasty=-1
    exit=False

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit = True
                break
        
        if (exit):
            break    
        x, y = screen.get_size()
        screen.blit(canvas, [0,0])
            
        if pygame.mouse.get_pressed()[0]:
            mx, my = pygame.mouse.get_pos()
            if (hover):
                temp = []
                drawInstructions.append(temp)
                drawNumber+=1
            else:
                pygame.draw.line(
                    canvas,
                    drawColor,
                    (lastx, lasty),
                    (mx, my),
                    brushSize*2

                )
            hover=False
            if (lastx!=mx and lasty!=my):
                drawInstructions[drawNumber].append([mx/width, my/height])
            lastx=mx
            lasty=my
            
            pygame.draw.circle(
                canvas,
                drawColor,
                [mx, my],
                brushSize,
            )
            
        if not pygame.mouse.get_pressed()[0]:
            hover=True
        
        pygame.display.flip()
        fpsClock.tick(fps)

    for i in range(len(drawInstructions)):
        for j in range(len(drawInstructions[i])):
            drawInstructions[i][j][0] *= -1
            drawInstructions[i][j][0] += 1

    for i in range(len(drawInstructions)):
        drawInstructions[i].insert(0, drawInstructions[i][0])
        drawInstructions[i].append(drawInstructions[i][-1])
    return drawInstructions
print(getDrawInstructions())