# Klasse for å tegne linjer
class Line:

    # Konstruktør
    def __init__(self, points, drawer):
        self.points = points
        self.drawer = drawer
    
    # Metode for å tegne en linje
    def draw(self):
        self.drawer.moveToPoint(self.points[0])
        self.drawer.lowerPen()
        for i in range(len(self.points)):
            self.drawer.moveToPoint(self.points[i])
        self.drawer.moveToPoint(self.points[-1])
        self.drawer.raisePen()