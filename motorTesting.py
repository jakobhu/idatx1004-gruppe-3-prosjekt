#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
from drawer import Drawer
from line import Line

# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()


# Write your program here.
ev3.speaker.beep()

penMotor = Motor(Port.B)
yMotor = Motor(Port.A)
xMotor = Motor(Port.C)

currentMotor = penMotor

speed = 200
delay = 300
run = False
runForward = False

while True:
    buttons = ev3.buttons.pressed()
    if Button.CENTER in buttons:
        run = not run
        wait(delay)
    elif Button.DOWN in buttons:
        speed *= -1
        wait(delay)
    elif Button.LEFT in buttons:
        currentMotor.stop()
        currentMotor = xMotor
        wait(delay)
    elif Button.UP in buttons:
        currentMotor.stop()
        currentMotor = yMotor
        wait(delay)
    elif Button.RIGHT in buttons:
        currentMotor.stop()
        currentMotor = penMotor
        wait(delay)

    if run:
        currentMotor.run(speed)
    if not run:
        currentMotor.stop()
        ev3.screen.print(currentMotor.angle())
